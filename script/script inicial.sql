﻿CREATE DATABASE dbprocessamentopg
  WITH OWNER = postgres
       ENCODING = 'UTF8'
       TABLESPACE = pg_default
       LC_COLLATE = 'Portuguese_Brazil.1252'
       LC_CTYPE = 'Portuguese_Brazil.1252'
       CONNECTION LIMIT = -1;

CREATE SCHEMA public
  AUTHORIZATION postgres;

GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO public;
COMMENT ON SCHEMA public
  IS 'standard public schema';


/* modelo_logico: */

CREATE TABLE usuario (
    id bigint PRIMARY KEY,
    nome character varying,
    login character varying,
    senha character varying,
    data_cadastro date,
    id_perfil bigint,
    UNIQUE (id, nome, login)
);

CREATE TABLE perfil (
    id bigint PRIMARY KEY,
    nome character varying,
    UNIQUE (id, nome)
);

CREATE TABLE aplicacao_captura (
    id bigint PRIMARY KEY,
    descricao character varying,
    UNIQUE (id, descricao)
);

CREATE TABLE tipo_arquivo (
    id bigint PRIMARY KEY,
    descricao character varying,
    UNIQUE (id, descricao)
);

CREATE TABLE configuracao_envio (
    id bigint PRIMARY KEY
    id_perfil bigint,
    id_aplicacao_captura bigint,
    id_tipo_arquivo bigint
);

CREATE TABLE arquivo_enviado (
    id bigint PRIMARY KEY,
    descricao_arquivo character varying,
    data_hora_envio timestamp,
    id_aplicacao_origem bigint,
    id_tipo_arquivo bigint,
    id_usuario_envio bigint,
    UNIQUE (descricao_arquivo)
);

CREATE TABLE public.contagem_palavra_envio
(
  id bigint,
  id_arquivo_enviado bigint,
  palavra character varying,
  numero_ocorrencias integer
)

ALTER TABLE public.contagem_palavra_envio
  OWNER TO postgres;

 
ALTER TABLE usuario ADD CONSTRAINT fk_perfil
    FOREIGN KEY (id_perfil)
    REFERENCES perfil (id);
 
ALTER TABLE configuracao_envio ADD CONSTRAINT fk_perfil_configuracao
    FOREIGN KEY (id_perfil)
    REFERENCES perfil (id);
 
ALTER TABLE configuracao_envio ADD CONSTRAINT fk_captura_configuracao
    FOREIGN KEY (id_aplicacao_captura)
    REFERENCES aplicacao_captura (id);
 
ALTER TABLE configuracao_envio ADD CONSTRAINT FK_configuracao_envio_2
    FOREIGN KEY (id_tipo_arquivo)
    REFERENCES tipo_arquivo (id);
 
ALTER TABLE arquivo_enviado ADD CONSTRAINT fk_envio_captura
    FOREIGN KEY (id_aplicacao_origem)
    REFERENCES aplicacao_captura (id);
 
ALTER TABLE arquivo_enviado ADD CONSTRAINT fk_envio_tipo_arquivo
    FOREIGN KEY (id_tipo_arquivo)
    REFERENCES tipo_arquivo (id);
 
ALTER TABLE arquivo_enviado ADD CONSTRAINT fk_envio_usuario
    FOREIGN KEY (id_usuario_envio)
    REFERENCES usuario (id);
	
-- Carga Inicial
INSERT INTO public.perfil(
            id, nome)
    VALUES (1, 'Administrador');

INSERT INTO public.perfil(
            id, nome)
    VALUES (2, 'Operador');

-- Criação  de usuário administrador com senha teste
INSERT INTO public.usuario(
            id, nome, login, senha, data_cadastro, id_perfil)
    VALUES (1, 'Administrador', 'admin', '2e6f9b0d5885b6010f9167787445617f553a735f', now(), 1);


-- Criação  de usuário operador com senha operador
 INSERT INTO public.usuario(
            id, nome, login, senha, data_cadastro, id_perfil)
    VALUES (2, 'Operador Fulano', 'operador01', '9d24de3ac7b5fbbe776a6d90fe25a7e3c74a29cc', now(), 2);

 INSERT INTO public.aplicacao_captura(
            id, descricao)
    VALUES (1, 'WEB');

    INSERT INTO public.aplicacao_captura(
            id, descricao)
    VALUES (2, 'DESKTOP');

     INSERT INTO public.aplicacao_captura(
            id, descricao)
    VALUES (3, 'MOBILE');

INSERT INTO public.tipo_arquivo(
            id, descricao)
    VALUES (1, 'TXT');

 INSERT INTO public.tipo_arquivo(
            id, descricao)
    VALUES (2, 'JPG');

  INSERT INTO public.tipo_arquivo(
            id, descricao)
    VALUES (3, 'PDF');

 CREATE SEQUENCE public.usuario_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 3
  CACHE 1;
ALTER TABLE public.usuario_seq
  OWNER TO postgres;

     CREATE SEQUENCE public.perfil_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 3
  CACHE 1;
ALTER TABLE public.perfil_seq
  OWNER TO postgres;

    CREATE SEQUENCE public.aplicacao_captura_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 4
  CACHE 1;
ALTER TABLE public.aplicacao_captura_seq
  OWNER TO postgres;

  CREATE SEQUENCE public.tipo_arquivo_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 4
  CACHE 1;
ALTER TABLE public.tipo_arquivo_seq
  OWNER TO postgres;

 CREATE SEQUENCE public.arquivo_enviado_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE public.arquivo_enviado_seq
  OWNER TO postgres;

   CREATE SEQUENCE public.contagem_palavtra_envio_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE public.contagem_palavtra_envio_seq
  OWNER TO postgres;

