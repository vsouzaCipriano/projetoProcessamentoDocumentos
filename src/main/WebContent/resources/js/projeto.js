
$("#wrapper").toggleClass("toggled");
var elementoResponsavelEnvio;
var loginUsuarioAplicacao;
$(".botaoMenu").click(function(e) {
    e.preventDefault();
    var idElementoSubmenu = $(this)[0].nextElementSibling.id;
    if($("#"+idElementoSubmenu).attr("hidden")) {
    	$("#"+idElementoSubmenu).removeAttr('hidden');
    } else {
    	$("#"+idElementoSubmenu).attr("hidden",true);
    }        
});
    
    $("#btnListarUsuarios").click(function(e) {
        e.preventDefault();
        $(".exibicaoOpcional").attr("hidden",true);
    	$("#page-content-usuario").removeAttr('hidden');
    	carregarTabelaUsuarios();
    
    });
    
    $("#btnListarPerfis").click(function(e) {
        e.preventDefault();
        $(".exibicaoOpcional").attr("hidden",true);
    	$("#page-content-perfil").removeAttr('hidden');
    	carregarTabelaPerfis();
    
    });
    
    $("#btnListarEnvios").click(function(e) {
        e.preventDefault();
        $(".exibicaoOpcional").attr("hidden",true);
    	$("#page-content-envioRealizado").removeAttr('hidden');
    	carregarListaEnvios();
    	popularSelect("selectAplicacaoCapturaPesquisarEnvio","aplicacoesCaptura");
     	popularSelect("selectTipoArquivoPesquisarEnvio","tiposArquivo");
    
    });
    
    $("#btnListarTiposArquivo").click(function(e) {
        e.preventDefault();
        $(".exibicaoOpcional").attr("hidden",true);
    	$("#page-content-tipoArquivo").removeAttr('hidden');
    	carregarListaTiposArquivo();
    
    });
    
    
    $(".botaoAcaoEnvio").click(function(e) {
       
    	elementoResponsavelEnvio = $(this)[0].id;
    	e.preventDefault();
        $('#modalConfiguracaoEnvio').modal('show');
        $("#falhaAutenticao").attr("hidden",true);
		 $("#camposObrigatorios").attr("hidden",true);
		$("#loginUsuario").val("");
	    $("#senhaUsuario").val("");
		$("#loginUsuario").removeClass('alertaObrigatorio');
	    $("#senhaUsuario").removeClass('alertaObrigatorio');
    
    });
    
    $("#autenticarUsuario").click(function(e) {
        e.preventDefault();
    var login = $("#loginUsuario").val();
    var senha =    $("#senhaUsuario").val();
    
    if (login != '' && senha != '') {
    var usuarioObjeto = {login: login,senha:senha};

   	 $.ajax({
   	        url: "http://localhost:8080/SpringRestHibernateExample/autenticaUsuario/",
   	        contentType: "application/json; charset=utf-8",
	        type: 'POST',
   	        data: JSON.stringify(usuarioObjeto)
   	 }).then(function(data) { 
   		 
   		 if (data) {
   			loginUsuarioAplicacao = login;
   	        $('#modalConfiguracaoEnvio').modal('hide');
   	     $(".exibicaoOpcional").attr("hidden",true);
   	     
   	     if (elementoResponsavelEnvio == 'btnConfiguracoesEnvio') {
   	  	$("#page-content-configuracaoEnvio").removeAttr('hidden');
     	carregarTabelaConfiguracoesEnvio();
   	    	 
   	     } else {
   	   	  	if (elementoResponsavelEnvio == 'btnRealizarEnvio') {
   	    	 $("#page-content-realizarEnvio").removeAttr('hidden');
   	    	$("#spanLoginUsuario").text(login);
   	  	popularSelect("selectAplicacaoCapturaEnvio","aplicacoesCaptura");
     	popularSelect("selectTipoArquivoEnvio","tiposArquivo");
   	     } else {
   	    	 $("#page-content-listarEnvio").removeAttr('hidden');
   	 
   	     }
   	     }
     
     	popularSelect("selectPerfil","perfis");
     	popularSelect("selectAplicacaoCaptura","aplicacoesCaptura");
     	popularSelect("selectTipoArquivo","tiposArquivo");


   		 } else {
   	        $("#falhaAutenticacao").attr("hidden",false);
   		    $("#camposObrigatorios").attr("hidden",true);
   			$("#loginUsuario").removeClass('alertaObrigatorio');
   	    	$("#senhaUsuario").removeClass('alertaObrigatorio');
   			 
   		 }
   		 
   	     });
    } else {
	    $("#camposObrigatorios").attr("hidden",false);
	        $("#falhaAutenticao").attr("hidden",true);

    	$("#loginUsuario").addClass('alertaObrigatorio');
    	$("#senhaUsuario").addClass('alertaObrigatorio');

    }
    });
    
    
    $(document).on('click','.removePerfil',function(){
    	 $.ajax({
    	        url: "http://localhost:8080/SpringRestHibernateExample/removePerfil/"+$(this)[0].id,
    	        type: 'DELETE'
    	 }).then(function() { 
    	    	carregarTabelaUsuarios();
    	        alert("Removido com sucesso");		
    	        
    	     });
    });
    
    $(document).on('click','.removeUsuario',function(){
   	 $.ajax({
   	        url: "http://localhost:8080/SpringRestHibernateExample/removeUsuario/"+$(this)[0].id,
   	        type: 'DELETE'
   	 }).then(function() { 
   	    	carregarTabelaUsuarios();
   	        alert("Removido com sucesso");		
   	        
   	     });
   }); 
    
 $("#btnPesquisarEnvio").click(function(e) {
    	
 	var aplicacaoCapturaSelecionada = $("#selectAplicacaoCapturaPesquisarEnvio").val();
 	var tipoArquivoSelecionado = $("#selectTipoArquivoPesquisarEnvio").val();

 	var configuracaoEnvio = {idCaptura:aplicacaoCapturaSelecionada,idTipoArquivo:tipoArquivoSelecionado};
    	
    	 $.ajax({
    	        url: "http://localhost:8080/SpringRestHibernateExample/pesquisarEnvios/",
    	        type: 'POST',
    	        contentType: "application/json; charset=utf-8",
    	        data: JSON.stringify(configuracaoEnvio)
    	 }).then(function(data) { 
    		 var qtdRegistros = data.length;
    	        
    	        $("#tabelaEnviosRealizados tbody").html("");           
    	        for (i = 0;i<qtdRegistros;i++) {
    	     	   
    	            var markup = "<tr><td>" + new Date(data[i].dataHoraEnvio).toLocaleDateString() + "</td><td>" + data[i].aplicacaoCaptura.descricao + "</td><td>" + data[i].tipoArquivo.descricao + "</td></tr>";
    	            $("#tabelaEnviosRealizados tbody").append(markup);
    	        }
    	}) 
    });
    
    $("#btnSalvarNovoArquivo").click(function(e) {
    	
    	var formatoArquivo = $("#descricaoArquivo").val().toUpperCase();
    	
    	if (formatoArquivo != '') {
    	var tipoArquivo = {descricao: formatoArquivo,id:null};

    	 $.ajax({
    	        url: "http://localhost:8080/SpringRestHibernateExample/adicionaTipoArquivo/",
    	        type: 'POST',
    	        contentType: "application/json; charset=utf-8",
    	        data: JSON.stringify(tipoArquivo)
    	 }).then(function() { 
    		 carregarListaTiposArquivo();
    	        alert("Inserido com sucesso");		
    	        $("#descricaoArquivo").val("");
    	     });
    	} else {
    		alert("Campo descricao obrigatorio");
    	}
    });
    
 $("#btnSalvarNovaConfiguracao").click(function(e) {
    	
    	var perfilSelecionado = $("#selectPerfil").val();
    	var aplicacaoCapturaSelecionada = $("#selectAplicacaoCaptura").val();
    	var tipoArquivoSelecionado = $("#selectTipoArquivo").val();

    	var configuracaoEnvio = {idPerfil: perfilSelecionado,idCaptura:aplicacaoCapturaSelecionada,idTipoArquivo:tipoArquivoSelecionado};

    	 $.ajax({
    	        url: "http://localhost:8080/SpringRestHibernateExample/adicionaConfiguracaoEnvio/",
    	        type: 'POST',
    	        contentType: "application/json; charset=utf-8",
    	        data: JSON.stringify(configuracaoEnvio)
    	 }).then(function() { 
    		 carregarTabelaConfiguracoesEnvio();
    	        alert("Inserido com sucesso");		
    	     });
    	
    });
 
 $("#btnRegistrarEnvio").click(function(e) {
	 e.preventDefault();

	 var usuarioEnvio = loginUsuarioAplicacao;
 	var aplicacaoCapturaSelecionada = $("#selectAplicacaoCapturaEnvio").val();
 	var tipoArquivoSelecionado = $("#selectTipoArquivoEnvio").val();

 	var envio = {loginUsuario: usuarioEnvio,idCaptura:aplicacaoCapturaSelecionada,idTipoArquivo:tipoArquivoSelecionado};

 	 $.ajax({
 	        url: "http://localhost:8080/SpringRestHibernateExample/validarPermissaoUsuarioEnvio/",
 	        type: 'POST',
 	        contentType: "application/json; charset=utf-8",
 	        data: JSON.stringify(envio)
 	 }).then(function(data) { 
 		 if (data) {
 			 var arquivo = document.getElementById("arquivo").files;
 			 if (arquivo.length > 0) {
 	 			 
 		 		   $.ajax({
 		 	 	        url: "http://localhost:8080/SpringRestHibernateExample/enviarArquivo/",
 		 	 	        type: 'POST',
 		 	 	        processData: false,
 		 	 	        contentType: false,
 		 	 	     enctype: 'multipart/form-data',
 		 	 	        data: new FormData($("#upload-file-form")[0])
 		 	 	 }).then(function(data) { 
 		 	 		 
 		 	 	 })
 			 } else {
 	 			 alert("Escolha um arquivo")

 			 }
 			
 			 
 		 }	else {
 			 alert("Sem permissao para realizar este envio")
 		 }
 	     });
 	
 });


 function carregarListaEnvios() {

 	$.ajax({
         url: "http://localhost:8080/SpringRestHibernateExample/enviosRealizados/"
     }).then(function(data) { 
        var qtdRegistros = data.length;
        
        $("#tabelaEnviosRealizados tbody").html("");           
        for (i = 0;i<qtdRegistros;i++) {
     	   
            var markup = "<tr><td>" + new Date(data[i].dataHoraEnvio).toLocaleDateString() + "</td><td>" + data[i].aplicacaoCaptura.descricao + "</td><td>" + data[i].tipoArquivo.descricao + "</td></tr>";
            $("#tabelaEnviosRealizados tbody").append(markup);
        }
        
     });
 }
 
    function carregarTabelaUsuarios() {

    	$.ajax({
            url: "http://localhost:8080/SpringRestHibernateExample/usuarios/"
        }).then(function(data) { 
           var qtdRegistros = data.length;
           
           $("#tabelaUsuarios tbody").html("");           
           for (i = 0;i<qtdRegistros;i++) {
        	   
               var markup = "<tr><td>" + data[i].nome + "</td><td>" + data[i].login + "</td><td>" + data[i].perfil.nome + "</td><td class='text-center'><button id='" + data[i].id+ "'type='button' class='btn-xs btn-danger removeUsuario'>X</button></td></tr>";
               $("#tabelaUsuarios tbody").append(markup);
           }
           
        });
    }
    
    function carregarTabelaPerfis() {

    	$.ajax({
            url: "http://localhost:8080/SpringRestHibernateExample/perfis/"
        }).then(function(data) { 
           var qtdRegistros = data.length;
           
           $("#tabelaPerfis tbody").html("");           
           for (i = 0;i<qtdRegistros;i++) {
        	   
               var markup = "<tr><td>" + data[i].nome + "</td><td class='text-center'><button id='" + data[i].id+ "'type='button' class='btn-xs btn-danger removePerfil'>X</button></td></tr>";
               $("#tabelaPerfis tbody").append(markup);
           }
           
        });
    }
    
    function carregarListaTiposArquivo() {

    	$.ajax({
            url: "http://localhost:8080/SpringRestHibernateExample/tiposArquivo/"
        }).then(function(data) { 
           var qtdRegistros = data.length;
           
           $("#listaTiposArquivo").html("");           
           for (i = 0;i<qtdRegistros;i++) {
        	   
               var markup = "<li class='itemLista'>" + data[i].descricao + "</li>";
               $("#listaTiposArquivo").append(markup);
           }
           
        });
    }
    
    function carregarTabelaConfiguracoesEnvio() {

    	$.ajax({
            url: "http://localhost:8080/SpringRestHibernateExample/configuracoesEnvio/"
        }).then(function(data) { 
           var qtdRegistros = data.length;
           
           $("#tabelaConfiguracoesEnvio tbody").html("");           
           for (i = 0;i<qtdRegistros;i++) {
        	   
               var markup = "<tr><td>" + data[i].perfil.nome + "</td><td>" + data[i].aplicacaoCaptura.descricao + "</td><td>" + data[i].tipoArquivo.descricao + "</td>";
               $("#tabelaConfiguracoesEnvio tbody").append(markup);
           }
           
        });
    }
    
    function popularSelect(idSelect,objetoURL) {

    	$.ajax({
            url: "http://localhost:8080/SpringRestHibernateExample/"+objetoURL+"/"
        }).then(function(data) { 
           var qtdRegistros = data.length;
           
           $("#"+idSelect).html("");           
           for (i = 0;i<qtdRegistros;i++) {
        	  var nomeDescricao = "";
        	  if (data[i].nome != undefined) {
        		  nomeDescricao = data[i].nome;
        	  } else {
        		  nomeDescricao = data[i].descricao;
        	  }
        	  var opcao = new Option(nomeDescricao, data[i].id);
        	  $(opcao).html(nomeDescricao.toUpperCase());
        	 $("#"+idSelect).append(opcao);
           }
           
        });
    }
    
    
    $('#selectTipoArquivoEnvio').change(function() {
    	  $('#arquivo').attr("accept", "." + $("#selectTipoArquivoEnvio :selected").text());
    	})