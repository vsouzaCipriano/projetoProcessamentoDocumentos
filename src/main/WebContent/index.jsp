<!DOCTYPE html>
<html lang="pt">

<head>

<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="Vinicius de Souza Cipriano">

<title>Projeto</title>
<!-- Bootstrap core CSS -->
<link href="resources/css/bootstrap.min.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="resources/css/simple-sidebar.css" rel="stylesheet">
<link href="resources/css/projeto.css" rel="stylesheet">

</head>

<body>

	<!-- Modal -->
	<div class="modal fade" id="modalConfiguracaoEnvio" tabindex="-1"
		role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="modalConfiguracaoEnvioLabel">Autentica��o
						necess�ria</h5>
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" style="text-align: center;">
					Login:<br> <input type="text" id="loginUsuario"> <br>
					Senha:<br> <input type="password" id="senhaUsuario"> <br>
					<span id="camposObrigatorios" style="color: red;" hidden="true">Campos
						Obrigat�rios</span> <span id="falhaAutenticacao" style="color: red;"
						hidden="true">Falha na autentica��o</span>

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary"
						id="autenticarUsuario">Autenticar</button>
				</div>
			</div>
		</div>
	</div>


	<div id="wrapper">

		<!-- Sidebar -->
		<div id="sidebar-wrapper">
			<ul class="sidebar-nav">
				<li class="sidebar-brand"><a href="#"> Funcionalidades </a></li>
				<li><a href="#" id="btnMenuUsuario" data-toggle="collapse"
					class='botaoMenu' data-target="#submenuUsuario"
					aria-expanded="false">Usu�rios</a>
					<ul hidden="true" class="nav collapse" id="submenuUsuario"
						role="menu" aria-labelledby="btn-2" style="padding-left: 25px;">
						<li><a href="#" id="btnListarUsuarios">Listar Usu�rios</a></li>
					</ul></li>

				<li><a href="#" id="btnMenuPerfil" data-toggle="collapse"
					class='botaoMenu' data-target="#submenuPerfil"
					aria-expanded="false">Perfis</a>
					<ul hidden="true" class="nav collapse" id="submenuPerfil"
						role="menu" aria-labelledby="btn-2" style="padding-left: 25px;">
						<li><a href="#" id="btnListarPerfis">Listar Perfis</a></li>
					</ul></li>

				<li><a href="#" id="btnMenuEnvio" data-toggle="collapse"
					class='botaoMenu' data-target="#submenuEnvio" aria-expanded="false">Envios de documento</a>
					<ul hidden="true" class="nav collapse" id="submenuEnvio"
						role="menu" style="padding-left: 25px;">
						<li><a href="#" id="btnConfiguracoesEnvio" class='botaoAcaoEnvio'>Configura��es
								de envio</a></li>
						<li><a href="#" id="btnRealizarEnvio" class='botaoAcaoEnvio'>Contagem de
								palavras</a></li>
						<li><a href="#" id="btnListarEnvios">Envios realizados</a></li>
					</ul></li>

				<li><a href="#" id="btnListarTiposArquivo">Tipos de arquivo</a></li>



			</ul>
		</div>
		<!-- /#sidebar-wrapper -->

		<!-- Page Content -->
		<div id="page-content-wrapper" class="exibicaoOpcional">
			<div class="container-fluid" style="padding-left: 25px;">
				<h1>Bem vindo ao projeto de envio de documentos</h1>
				<p>Para acessar as funcionalidades da aplica��o utilize o menu
					lateral</p>
			</div>
		</div>
		<!-- /#page-content-wrapper -->


		<div id="page-content-usuario" hidden="true" class="exibicaoOpcional">
			<div class="container-listaUsuario" style="padding-left: 25px;">
				<h3>Lista de Usu�rios</h3>

				<table id="tabelaUsuarios" class="table">

					<thead>
						<tr>
							<th>Nome</th>
							<th>Login</th>
							<th>Perfil</th>
							<th class='text-center'>Remover Usu�rio</th>
						</tr>
					</thead>

					<tbody>
					</tbody>
				</table>
			</div>
		</div>

		<div id="page-content-perfil" hidden="true" class="exibicaoOpcional">
			<div class="container-listaPerfil" style="padding-left: 25px;">
				<h3>Lista de Perfis</h3>

				<table id="tabelaPerfis" class="table">

					<thead>
						<tr>
							<th>Nome</th>
							<th class='text-center'>Remover Perfil</th>
						</tr>
					</thead>

					<tbody>
					</tbody>
				</table>
			</div>
		</div>

		<div id="page-content-tipoArquivo" hidden="true"
			class="exibicaoOpcional">
			<div class="container-listaTipoArquivo" style="padding-left: 25px;">
				<strong><p>Adicione novos tipos de arquivo</p></strong> Descri��o:<br>
				<input type="text" id="descricaoArquivo"> <br> <br>
				<input type="submit" id="btnSalvarNovoArquivo"> <br> <br>
				<h5>Formatos de arquivos Permitidos para envio</h5>

				<ul id="listaTiposArquivo" class="listaTresColunas">

				</ul>
			</div>
		</div>

		<div id="page-content-configuracaoEnvio" hidden="true"
			class="exibicaoOpcional">

			<div style="padding-left: 25px;">
				<strong><p>Adicione novas configura��es</p></strong> Perfil:<br>
				<select id="selectPerfil" class="campoSelect"> 
				</select> <br>Aplica��o de origem:<br> <select id="selectAplicacaoCaptura" class="campoSelect">
				</select> <br>Tipo de arquivo:<br><select id="selectTipoArquivo" class="campoSelect">
				</select> <br> <br> <input type="submit"
					id="btnSalvarNovaConfiguracao"> <br> <br>

				<ul id="listaTiposArquivo" class="listaTresColunas">

				</ul>
			</div>
			<div class="container-listaConfiguracaoEnvio"
				style="padding-left: 25px;">
				<h3>Lista contendo os perfis que podem realizar um determinado
					tipo de envio</h3>

				<table id="tabelaConfiguracoesEnvio" class="table">

					<thead>
						<tr>
							<th>Perfil</th>
							<th>Aplica��o de origem</th>
							<th>Tipo do Arquivo</th>
						</tr>
					</thead>

					<tbody>
					</tbody>
				</table>
			</div>
		</div>
		
		<div id="page-content-realizarEnvio" hidden="true"
			class="exibicaoOpcional">
<form id="upload-file-form">

			<div style="padding-left: 25px;">
				<strong><p>Informe as caracter�sticas do envio</p></strong> <br>
				Envio realizado pelo usu�rio de login: <strong><span id="spanLoginUsuario"></span></strong> <br>				
			<br>Aplica��o de origem:<br> <select id="selectAplicacaoCapturaEnvio" class="campoSelect">
				</select> <br>Tipo de arquivo:<br><select id="selectTipoArquivoEnvio" class="campoSelect">
				</select> <br> <br>
				  <input type="file" id="arquivo" name="file">
				
				<br> <br> <input type="submit"
					id="btnRegistrarEnvio"> <br> <br>

			</div>
			</form>
			
		</div>
		
		<div id="page-content-envioRealizado" hidden="true"
			class="exibicaoOpcional">

<div style="padding-left: 25px;">
				<strong><p>Pesquisar</p></strong> <br>Aplica��o de origem:<br> <select id="selectAplicacaoCapturaPesquisarEnvio" class="campoSelect">
				</select> <br>Tipo de arquivo:<br><select id="selectTipoArquivoPesquisarEnvio" class="campoSelect">
				</select> <br> <br> <a href="#" id="btnPesquisarEnvio">Pesquisar</a> <br> <br>

			</div>

			<div class="container-listaEnviosRealizados"
				style="padding-left: 25px;">
				<h3>Lista contendo os envios realizados</h3>

				<table id="tabelaEnviosRealizados" class="table">

					<thead>
						<tr>
							<th>Data Envio</th>
							<th>Aplica��o de origem</th>
							<th>Tipo do Arquivo</th>
						</tr>
					</thead>

					<tbody>
					</tbody>
				</table>
			</div>
		</div>

	</div>
	<!-- /#wrapper -->

	<!-- Bootstrap core JavaScript -->
	<script src="resources/js/jquery.min.js"></script>
	<script src="resources/js/bootstrap.bundle.min.js"></script>
	<script src="resources/js/projeto.js" charset="utf-8"></script>

</body>

</html>