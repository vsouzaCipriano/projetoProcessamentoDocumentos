package com.service;


import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dao.UsuarioDAO;
import com.entidades.Usuario;

@Service("usuarioService")
public class UsuarioService {

	@Autowired
	UsuarioDAO usuarioDao;

	@Transactional
	public List<Usuario> listarUsuarios() {
		List<Usuario> lista =(List<Usuario>) usuarioDao.listar("Usuario");
		return lista;
	}

	@Transactional
	public Usuario pesquisarPorId(Long id) {
		Usuario usuario =(Usuario) usuarioDao.pesquisarPorId(id,"Usuario");
		return usuario;
	}

	@Transactional
	public List<Usuario> pesquisarPorNome(String nome) {
		List<Usuario> lista =usuarioDao.pesquisarPorNome(nome);
		return lista;
	}


	@Transactional
	public void adicionaUsuario(Usuario usuario) {
		usuarioDao.adicionar(usuario);
	}

	@Transactional
	public void atualizaUsuario(Usuario usuario) {
		usuarioDao.atualizar(usuario);

	}

	@Transactional
	public void removeUsuario(int id) {
		usuarioDao.remover(id, "Usuario");
	}

	@Transactional
	public Boolean autenticaUsuario(Usuario usuario) {
		Usuario usuarioBanco =usuarioDao.pesquisarPorLogin(usuario.
				getLogin());
		if (usuarioBanco == null) {
			return false;
		} else {
			if (!usuarioBanco.getPerfil().getId().equals(1L)) {
				return false;
			} else {
				if (!usuarioBanco.getSenha().equals(DigestUtils.sha1Hex(usuario.getSenha()))) {
					return false;
				}
			}
		}
		return true;
	}

}



