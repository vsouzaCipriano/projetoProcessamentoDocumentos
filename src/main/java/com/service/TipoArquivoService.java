package com.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dao.TipoArquivoDAO;
import com.entidades.TipoArquivo;

@Service("tipoArquivoService")
public class TipoArquivoService {

	@Autowired
	TipoArquivoDAO tipoArquivoDao;

	@Transactional
	public List<TipoArquivo> listarTiposArquivos() {
		List<TipoArquivo> lista =(List<TipoArquivo>) tipoArquivoDao.listar("TipoArquivo");
		return lista;
	}

	@Transactional
	public TipoArquivo pesquisarPorId(Long id) {
		TipoArquivo tipoArquivo =(TipoArquivo) tipoArquivoDao.pesquisarPorId(id,"TipoArquivo");
		return tipoArquivo;
	}

	@Transactional
	public void adicionaTipoArquivo(TipoArquivo tipoArquivo) {
		tipoArquivoDao.adicionar(tipoArquivo);
	}

}
