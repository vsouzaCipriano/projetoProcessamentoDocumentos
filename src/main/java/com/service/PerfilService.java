package com.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dao.PerfilDAO;
import com.entidades.Perfil;

@Service("perfilService")
public class PerfilService {

	@Autowired
	PerfilDAO perfilDao;

	@Transactional
	public List<Perfil> listarPerfils() {
		List<Perfil> lista =(List<Perfil>) perfilDao.listar("Perfil");
		return lista;
	}

	@Transactional
	public Perfil pesquisarPorId(Long id) {
		Perfil Perfil =(Perfil) perfilDao.pesquisarPorId(id,"Perfil");
		return Perfil;
	}

	@Transactional
	public List<Perfil> pesquisarPorNome(String nome) {
		List<Perfil> lista =perfilDao.pesquisarPorNome(nome);
		return lista;
	}


	@Transactional
	public void adicionaPerfil(Perfil Perfil) {
		perfilDao.adicionar(Perfil);
	}

	@Transactional
	public void atualizaPerfil(Perfil Perfil) {
		perfilDao.atualizar(Perfil);

	}

	@Transactional
	public void removePerfil(int id) {
		perfilDao.remover(id, "Perfil");
	}
}
