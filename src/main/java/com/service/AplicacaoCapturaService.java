package com.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dao.AplicacaoCapturaDAO;
import com.entidades.AplicacaoCaptura;

@Service("aplicacaoCapturaService")
public class AplicacaoCapturaService {

	@Autowired
	AplicacaoCapturaDAO aplicacaoCapturaDao;

	@Transactional
	public List<AplicacaoCaptura> listarAplicacoesCaptura() {
		List<AplicacaoCaptura> lista =(List<AplicacaoCaptura>) aplicacaoCapturaDao.listar("AplicacaoCaptura");
		return lista;
	}

	@Transactional
	public AplicacaoCaptura pesquisarPorId(Long id) {
		AplicacaoCaptura AplicacaoCaptura =(AplicacaoCaptura) aplicacaoCapturaDao.pesquisarPorId(id,"AplicacaoCaptura");
		return AplicacaoCaptura;
	}

	@Transactional
	public void adicionaAplicacaoCaptura(AplicacaoCaptura AplicacaoCaptura) {
		aplicacaoCapturaDao.adicionar(AplicacaoCaptura);
	}

}
