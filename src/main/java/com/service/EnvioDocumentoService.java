package com.service;


import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dao.AplicacaoCapturaDAO;
import com.dao.EnvioDocumentoDAO;
import com.dao.PerfilDAO;
import com.dao.TipoArquivoDAO;
import com.dao.UsuarioDAO;
import com.entidades.AplicacaoCaptura;
import com.entidades.ConfiguracaoEnvio;
import com.entidades.ContagemPalavraEnvio;
import com.entidades.Envio;
import com.entidades.Perfil;
import com.entidades.TipoArquivo;

@Service("envioDocumentoService")
public class EnvioDocumentoService {

	@Autowired
	EnvioDocumentoDAO envioDocumentoDAO;

	@Autowired
	PerfilDAO perfilDAO;

	@Autowired
	TipoArquivoDAO tipoArquivoDAO;

	@Autowired
	AplicacaoCapturaDAO aplicacaoCapturaDAO;

	@Autowired
	UsuarioDAO usuarioDAO;

	@Transactional
	public List<ConfiguracaoEnvio> listarConfiguracoesEnvio() {
		List<ConfiguracaoEnvio> lista =envioDocumentoDAO.listarConfiguracoesEnvio();
		return lista;
	}

	@Transactional
	public List<Envio> listarEnvios() {
		List<Envio> lista =envioDocumentoDAO.listarEnvios();
		return lista;
	}


	@Transactional
	public void adicionaConfiguracaoEnvio(Envio envio) {
		ConfiguracaoEnvio configuracaoEnvio = new ConfiguracaoEnvio();
		configuracaoEnvio.setPerfil((Perfil) this.perfilDAO.pesquisarPorId(envio.getIdPerfil(), "Perfil"));
		configuracaoEnvio.setTipoArquivo((TipoArquivo) this.tipoArquivoDAO.pesquisarPorId(envio.getIdTipoArquivo(), "TipoArquivo"));
		configuracaoEnvio.setAplicacaoCaptura((AplicacaoCaptura) this.aplicacaoCapturaDAO.pesquisarPorId(envio.getIdCaptura(), "AplicacaoCaptura"));

		envioDocumentoDAO.adicionar(configuracaoEnvio);
	}

	@Transactional
	public List<Envio> pesquisarEnvios(Envio envio) {

		return envioDocumentoDAO.pesquisarEnvios(envio);
	}

	@Transactional
	public void salvarContagemPalavras(Map<String, Integer> mapPalavraQuantidade,Envio envio,String nomeArquivo) {

		Envio envioArquivo = new Envio();
		envioArquivo.setUsuario(this.usuarioDAO.pesquisarPorLogin(envio.getLoginUsuario()));
		envioArquivo.setTipoArquivo((TipoArquivo) this.tipoArquivoDAO.pesquisarPorId(envio.getIdTipoArquivo(), "TipoArquivo"));
		envioArquivo.setAplicacaoCaptura((AplicacaoCaptura) this.aplicacaoCapturaDAO.pesquisarPorId(envio.getIdCaptura(), "AplicacaoCaptura"));
		envioArquivo.setNomeArquivo(nomeArquivo);
		envioArquivo.setDataHoraEnvio(Calendar.getInstance().getTime());;

		envioDocumentoDAO.adicionar(envioArquivo);
		Iterator it = mapPalavraQuantidade.entrySet().iterator();
		while (it.hasNext()) {
			ContagemPalavraEnvio contagemPalavraEnvio = new ContagemPalavraEnvio();
			Map.Entry par = (Map.Entry)it.next();
			if (!((String) par.getKey()).isEmpty()) {
				contagemPalavraEnvio.setEnvio(envioArquivo);
				contagemPalavraEnvio.setPalavra((String) par.getKey());
				contagemPalavraEnvio.setNumeroOcorrencias((Integer) par.getValue());
				envioDocumentoDAO.adicionar(contagemPalavraEnvio);
			}
		}
	}

	@Transactional
	public Boolean validarPermissaoUsuarioEnvio(Envio envio) {

		List<ConfiguracaoEnvio> listaConfiguracao = envioDocumentoDAO.pesquisarConfiguracaoPermitida(envio);

		if (listaConfiguracao != null && !listaConfiguracao.isEmpty()) {
			return true;
		} else {
			return false;
		}
	}

}
