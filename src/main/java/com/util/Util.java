package com.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.Parser;
import org.apache.tika.parser.ocr.TesseractOCRConfig;
import org.apache.tika.parser.pdf.PDFParserConfig;
import org.apache.tika.sax.BodyContentHandler;
import org.xml.sax.SAXException;



public class Util {



	public static String extrairTextoArquivo(File file) throws Exception {
		InputStream fileStream = new FileInputStream(file);
		Parser parser = new AutoDetectParser();
		Metadata metadata = new Metadata();
		BodyContentHandler handler = new BodyContentHandler(Integer.MAX_VALUE);

		TesseractOCRConfig config = new TesseractOCRConfig();
		PDFParserConfig pdfConfig = new PDFParserConfig();
		pdfConfig.setExtractInlineImages(true);

		ParseContext parseContext = new ParseContext();
		parseContext.set(TesseractOCRConfig.class, config);
		parseContext.set(PDFParserConfig.class, pdfConfig);
		parseContext.set(Parser.class, parser);
		try {
			parser.parse(fileStream, handler, metadata, parseContext);
			String text = handler.toString();

			return text;
		} catch (IOException | SAXException | TikaException e) {
			throw new Exception("N�o foi poss�vei extrair o texto", e);
		} finally {
			try {
				fileStream.close();
			} catch (IOException e) {
				throw new Exception(e);
			}
		}
	}

	public static Map<String, Integer> contaPalavraTexto(String texto) throws IOException {
		texto = texto.replaceAll("[^a-zA-Z\\s������������������������������������������������ܟ���������]","");

		String[] palavrasSeparadas = texto.toLowerCase().split(" ");

		Map<String, Integer> hm = new HashMap<String, Integer>();

		for (int i=0; i<palavrasSeparadas.length ; i++) {
			if (hm.containsKey(palavrasSeparadas[i])) {
				int cont = hm.get(palavrasSeparadas[i]);
				hm.put(palavrasSeparadas[i], cont + 1);
			} else {
				hm.put(palavrasSeparadas[i], 1);
			}
		}

		return hm;

	}

	public static XSSFWorkbook gerarRelatorioContagemPalavras(Map<String, Integer> mapPalavraQuantidade) throws IOException {
		XSSFWorkbook pastaTrabalho = new XSSFWorkbook();
		XSSFSheet sheet = pastaTrabalho.createSheet("ContagemPalavras");

		int contadorLinha = 1;
		Row linha = sheet.createRow(1);

		Cell celulaPalavra = linha.createCell(1);
		celulaPalavra.setCellValue("Palavra");
		Cell celulaNumeroOcorrencias = linha.createCell(2);
		celulaNumeroOcorrencias.setCellValue("N�mero de Ocorr�ncias");

		Iterator it = mapPalavraQuantidade.entrySet().iterator();
		while (it.hasNext()) {
			linha = sheet.createRow(++contadorLinha);

			Map.Entry par = (Map.Entry)it.next();
			if (!((String) par.getKey()).isEmpty()) {
				celulaPalavra = linha.createCell(1);
				celulaPalavra.setCellValue((String) par.getKey());
				celulaNumeroOcorrencias = linha.createCell(2);
				celulaNumeroOcorrencias.setCellValue((Integer) par.getValue());
			}

		}
		return pastaTrabalho;

	}

}
