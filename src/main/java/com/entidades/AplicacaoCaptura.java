package com.entidades;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name="aplicacao_captura")
public class AplicacaoCaptura{

	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="aplicacao_captura_seq")
	@SequenceGenerator(
			name="aplicacao_captura_seq",
			sequenceName="aplicacao_captura_seq",
			allocationSize=1
			)
	private Long id;

	@Column(name="descricao")
	private String descricao;

	public AplicacaoCaptura() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

}