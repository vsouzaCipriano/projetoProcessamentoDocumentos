package com.entidades;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "configuracao_envio")
public class ConfiguracaoEnvio {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "configuracao_envio_seq")
	@SequenceGenerator(name = "configuracao_envio_seq", sequenceName = "configuracao_envio_seq", allocationSize = 1)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "id_perfil")
	private Perfil perfil;

	@ManyToOne
	@JoinColumn(name = "id_aplicacao_captura")
	private AplicacaoCaptura aplicacaoCaptura;

	@ManyToOne
	@JoinColumn(name = "id_tipo_arquivo")
	private TipoArquivo tipoArquivo;



	public ConfiguracaoEnvio() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Perfil getPerfil() {
		return perfil;
	}

	public void setPerfil(Perfil perfil) {
		this.perfil = perfil;
	}

	public AplicacaoCaptura getAplicacaoCaptura() {
		return aplicacaoCaptura;
	}

	public void setAplicacaoCaptura(AplicacaoCaptura aplicacaoCaptura) {
		this.aplicacaoCaptura = aplicacaoCaptura;
	}

	public TipoArquivo getTipoArquivo() {
		return tipoArquivo;
	}

	public void setTipoArquivo(TipoArquivo tipoArquivo) {
		this.tipoArquivo = tipoArquivo;
	}


}