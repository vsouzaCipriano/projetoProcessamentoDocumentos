package com.entidades;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;


@Entity
@Table(name="tipo_arquivo")
public class TipoArquivo{

	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="tipo_arquivo_seq")
	@SequenceGenerator(
			name="tipo_arquivo_seq",
			sequenceName="tipo_arquivo_seq",
			allocationSize=1
			)
	private Long id;

	@JsonProperty
	@Column(name="descricao")
	private String descricao;

	public TipoArquivo() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

}