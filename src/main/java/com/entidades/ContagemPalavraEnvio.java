package com.entidades;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "contagem_palavra_envio")
public class ContagemPalavraEnvio {


	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "contagem_palavra_envio_seq")
	@SequenceGenerator(name = "contagem_palavra_envio_seq", sequenceName = "contagem_palavra_envio_seq", allocationSize = 1)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "id_arquivo_enviado")
	private Envio envio;

	@Column(name="palavra")
	private String palavra;

	@Column(name="numero_ocorrencias")
	private Integer numeroOcorrencias;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Envio getEnvio() {
		return envio;
	}

	public void setEnvio(Envio envio) {
		this.envio = envio;
	}

	public String getPalavra() {
		return palavra;
	}

	public void setPalavra(String palavra) {
		this.palavra = palavra;
	}

	public Integer getNumeroOcorrencias() {
		return numeroOcorrencias;
	}

	public void setNumeroOcorrencias(Integer numeroOcorrencias) {
		this.numeroOcorrencias = numeroOcorrencias;
	}

}
