package com.entidades;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "arquivo_enviado")
public class Envio {


	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "arquivo_enviado_seq")
	@SequenceGenerator(name = "arquivo_enviado_seq", sequenceName = "arquivo_enviado_seq", allocationSize = 1)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "id_aplicacao_origem")
	private AplicacaoCaptura aplicacaoCaptura;

	@Column(name="descricao_arquivo")
	private String nomeArquivo;

	@ManyToOne
	@JoinColumn(name = "id_tipo_arquivo")
	private TipoArquivo tipoArquivo;

	@ManyToOne
	@JoinColumn(name = "id_usuario_envio")
	private Usuario Usuario;

	@Column(name="data_hora_envio")
	@DateTimeFormat(pattern = "dd/mm/yyyy HH:mm:ss")
	private Date dataHoraEnvio;

	@Transient
	@JsonProperty
	private Long idPerfil;

	@Transient
	@JsonProperty
	private Long idCaptura;

	@Transient
	@JsonProperty
	private Long idTipoArquivo;

	@Transient
	@JsonProperty
	private String loginUsuario;

	public Long getIdPerfil() {
		return idPerfil;
	}

	public void setIdPerfil(Long idPerfil) {
		this.idPerfil = idPerfil;
	}

	public Long getIdCaptura() {
		return idCaptura;
	}

	public void setIdCaptura(Long idCaptura) {
		this.idCaptura = idCaptura;
	}

	public Long getIdTipoArquivo() {
		return idTipoArquivo;
	}

	public void setIdTipoArquivo(Long idTipoArquivo) {
		this.idTipoArquivo = idTipoArquivo;
	}

	public String getLoginUsuario() {
		return loginUsuario;
	}

	public void setLoginUsuario(String loginUsuario) {
		this.loginUsuario = loginUsuario;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public AplicacaoCaptura getAplicacaoCaptura() {
		return aplicacaoCaptura;
	}

	public void setAplicacaoCaptura(AplicacaoCaptura aplicacaoCaptura) {
		this.aplicacaoCaptura = aplicacaoCaptura;
	}

	public String getNomeArquivo() {
		return nomeArquivo;
	}

	public void setNomeArquivo(String nomeArquivo) {
		this.nomeArquivo = nomeArquivo;
	}

	public TipoArquivo getTipoArquivo() {
		return tipoArquivo;
	}

	public void setTipoArquivo(TipoArquivo tipoArquivo) {
		this.tipoArquivo = tipoArquivo;
	}

	public Usuario getUsuario() {
		return Usuario;
	}

	public void setUsuario(Usuario usuario) {
		Usuario = usuario;
	}

	public Date getDataHoraEnvio() {
		return dataHoraEnvio;
	}

	public void setDataHoraEnvio(Date dataHoraEnvio) {
		this.dataHoraEnvio = dataHoraEnvio;
	}


}
