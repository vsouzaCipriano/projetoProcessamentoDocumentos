package com.dao;



import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.entidades.Perfil;

@Repository
public class PerfilDAO extends GenericDAO {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	public List<Perfil> pesquisarPorNome(String nome) {
		Session session = this.sessionFactory.getCurrentSession();
		List<Perfil> lista = session.createQuery("from Perfil where lower(nome) like '%"+nome.toLowerCase()+"%'").list();
		return lista;
	}


}
