package com.dao;



import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.entidades.Usuario;

@Repository
public class UsuarioDAO extends GenericDAO {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	public List<Usuario> pesquisarPorNome(String nome) {
		Session session = this.sessionFactory.getCurrentSession();
		List<Usuario> lista = session.createQuery("from Usuario where lower(nome) like '%"+nome.toLowerCase()+"%'").list();
		return lista;
	}


	public Usuario pesquisarPorLogin(String login) {
		Session session = this.sessionFactory.getCurrentSession();
		Usuario usuario = (Usuario) session.createQuery("from Usuario where login = '"+login+"'").list().get(0);
		return usuario;
	}

}
