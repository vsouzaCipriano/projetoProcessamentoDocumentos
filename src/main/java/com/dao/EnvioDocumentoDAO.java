package com.dao;



import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.entidades.ConfiguracaoEnvio;
import com.entidades.Envio;


@Repository
public class EnvioDocumentoDAO extends GenericDAO {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	public List<Envio> listarEnvios() {
		Session session = this.sessionFactory.getCurrentSession();
		List<Envio> lista = session.createQuery("from Envio order by dataHoraEnvio asc").list();
		return lista;
	}

	public List<ConfiguracaoEnvio> listarConfiguracoesEnvio() {
		Session session = this.sessionFactory.getCurrentSession();
		List<ConfiguracaoEnvio> lista = session.createQuery("from ConfiguracaoEnvio order by perfil.nome asc,aplicacaoCaptura.descricao asc,tipoArquivo.descricao asc").list();
		return lista;
	}

	public List<ConfiguracaoEnvio> pesquisarConfiguracaoPermitida(Envio envio) {
		Session session = this.sessionFactory.getCurrentSession();
		List<ConfiguracaoEnvio> lista =  session.createQuery("from ConfiguracaoEnvio where aplicacaoCaptura.id =" + envio.getIdCaptura() + " and tipoArquivo.id=" + envio.getIdTipoArquivo() + " and perfil.id in (select perfil.id from Usuario where login='" +envio.getLoginUsuario()+ "')").list();
		return lista;
	}

	public List<Envio> pesquisarEnvios(Envio envio) {
		Session session = this.sessionFactory.getCurrentSession();
		List<Envio> lista =  session.createQuery("from Envio where aplicacaoCaptura.id =" + envio.getIdCaptura() + " and tipoArquivo.id=" + envio.getIdTipoArquivo()).list();
		return lista;
	}

}
