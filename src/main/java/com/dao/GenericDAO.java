package com.dao;


import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class GenericDAO {

	@Autowired
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	public List<?> listar(String classe) {
		Session session = this.sessionFactory.getCurrentSession();
		List<?> lista = session.createQuery("from "+ classe).list();
		return lista;
	}


	public Object pesquisarPorId(Long id,String classe) {
		Session session = this.sessionFactory.getCurrentSession();
		Object objeto = session.createQuery("from "+ classe +" where id="+id).list().get(0);
		return objeto;
	}

	public void adicionar(Object objeto) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(objeto);
	}

	public void atualizar(Object objeto) {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(objeto);
	}

	public void remover(int id,String classe) {
		Session session = this.sessionFactory.getCurrentSession();
		session.createQuery("delete from "+ classe +" where id="+id).executeUpdate();
	}
}
