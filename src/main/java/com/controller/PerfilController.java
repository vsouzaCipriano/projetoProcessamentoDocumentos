package com.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.entidades.Perfil;
import com.service.PerfilService;

@RestController
public class PerfilController {

	@Autowired
	PerfilService perfilService;

	@RequestMapping(value = "/perfis", method = RequestMethod.GET, headers = "Accept=application/json")
	public List<Perfil> listar() {

		List<Perfil> lista = perfilService.listarPerfils();
		return lista;
	}

	@RequestMapping(value = "/perfil/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
	public Perfil pesquisarPerfilPorId(@PathVariable Long id) {
		return perfilService.pesquisarPorId(id);
	}

	@RequestMapping(value = "/perfilPorNome/{nome}", method = RequestMethod.GET, headers = "Accept=application/json")
	public List<Perfil> pesquisarPerfilPorNome(@PathVariable String nome) {
		return perfilService.pesquisarPorNome(nome);
	}


	@RequestMapping(value = "/adicionaPerfil", method = RequestMethod.POST, headers = "Accept=application/json")
	public void adicionaPerfil(@RequestBody Perfil perfil) {
		perfilService.adicionaPerfil(perfil);

	}

	@RequestMapping(value = "/atualizaPerfil", method = RequestMethod.PUT, headers = "Accept=application/json")
	public void atualizaperfil(@RequestBody Perfil perfil) {
		perfilService.atualizaPerfil(perfil);
	}

	@RequestMapping(value = "/removePerfil/{id}", method = RequestMethod.DELETE, headers = "Accept=application/json")
	public void removPerfil(@PathVariable("id") int id) {
		perfilService.removePerfil(id);
	}
}
