package com.controller;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.entidades.ConfiguracaoEnvio;
import com.entidades.Envio;
import com.service.EnvioDocumentoService;
import com.util.Util;

@RestController
public class EnvioDocumentoController {

	@Autowired
	HttpSession session;


	@Autowired
	EnvioDocumentoService envioDocumentoService;

	@RequestMapping(value = "/configuracoesEnvio", method = RequestMethod.GET, headers = "Accept=application/json")
	public List<ConfiguracaoEnvio> listar() {

		List<ConfiguracaoEnvio> lista = envioDocumentoService.listarConfiguracoesEnvio();
		return lista;
	}

	@RequestMapping(value = "/enviosRealizados", method = RequestMethod.GET, headers = "Accept=application/json")
	public List<Envio> listarEnvios() {

		List<Envio> lista = envioDocumentoService.listarEnvios();
		return lista;
	}

	@RequestMapping(value = "/adicionaConfiguracaoEnvio", method = RequestMethod.POST, headers = "Accept=application/json")
	public void adicionaenvioDocumento(@RequestBody Envio envio) {
		envioDocumentoService.adicionaConfiguracaoEnvio(envio);

	}

	@RequestMapping(value = "/pesquisarEnvios", method = RequestMethod.POST, headers = "Accept=application/json")
	public List<Envio> pesquisarEnvios(@RequestBody Envio envio) {
		return envioDocumentoService.pesquisarEnvios(envio);

	}


	@RequestMapping(value = "/validarPermissaoUsuarioEnvio", method = RequestMethod.POST, headers = "Accept=application/json")
	public Boolean validarPermissaoUsuarioEnvio(@RequestBody Envio envio) {
		session.setAttribute("envio",envio);

		return envioDocumentoService.validarPermissaoUsuarioEnvio(envio);

	}

	@RequestMapping(value = "/enviarArquivo", method = RequestMethod.POST)
	@ResponseBody
	public void uploadArquivo(@RequestParam("file") MultipartFile uploadfile,HttpServletResponse response) {
		try {
			Envio envio = (Envio) this.session.getAttribute("envio");
			File file = new File(uploadfile.getOriginalFilename());
			file.createNewFile();
			FileOutputStream fos = new FileOutputStream(file);
			fos.write(uploadfile.getBytes());
			fos.close();
			String texto = Util.extrairTextoArquivo(file);
			Map<String, Integer> mapPalavraQuantidade= Util.contaPalavraTexto(texto);
			envioDocumentoService.salvarContagemPalavras(mapPalavraQuantidade,envio,uploadfile.getOriginalFilename());
			//Gerando arquivo em excel
			XSSFWorkbook pastaTrabalho = Util.gerarRelatorioContagemPalavras(mapPalavraQuantidade);

			response.setContentType("application/vnd.ms-excel");
			response.setHeader("Content-Disposition", "attachment; filename=contagemPalavras.xls");
			ByteArrayOutputStream outByteStream = new ByteArrayOutputStream();
			pastaTrabalho.write(outByteStream);

			byte[] outArray = outByteStream.toByteArray();
			OutputStream outStream = response.getOutputStream();
			outStream.write(outArray);
			outStream.flush();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


}
