package com.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.entidades.TipoArquivo;
import com.service.TipoArquivoService;

@RestController
public class TipoArquivoController {

	@Autowired
	TipoArquivoService tipoArquivoService;

	@RequestMapping(value = "/tiposArquivo", method = RequestMethod.GET, headers = "Accept=application/json")
	public List<TipoArquivo> listar() {

		List<TipoArquivo> lista = tipoArquivoService.listarTiposArquivos();
		return lista;
	}

	@RequestMapping(value = "/tipoArquivo/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
	public TipoArquivo pesquisarTipoArquivoPorId(@PathVariable Long id) {
		return tipoArquivoService.pesquisarPorId(id);
	}

	@RequestMapping(value = "/adicionaTipoArquivo", method = RequestMethod.POST, headers = "Accept=application/json")
	public void adicionaTipoArquivo(@RequestBody TipoArquivo tipoArquivo) {
		tipoArquivoService.adicionaTipoArquivo(tipoArquivo);

	}
}
