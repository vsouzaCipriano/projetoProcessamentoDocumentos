package com.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.entidades.Usuario;
import com.service.UsuarioService;

@RestController
public class UsuarioController {

	@Autowired
	UsuarioService usuarioService;

	@RequestMapping(value = "/usuarios", method = RequestMethod.GET, headers = "Accept=application/json")
	public List<Usuario> listar() {

		List<Usuario> lista = usuarioService.listarUsuarios();
		return lista;
	}

	@RequestMapping(value = "/usuario/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
	public Usuario pesquisarUsuarioPorId(@PathVariable Long id) {
		return usuarioService.pesquisarPorId(id);
	}

	@RequestMapping(value = "/usuarioPorNome/{nome}", method = RequestMethod.GET, headers = "Accept=application/json")
	public List<Usuario> pesquisarUsuarioPorNome(@PathVariable String nome) {
		return usuarioService.pesquisarPorNome(nome);
	}

	@RequestMapping(value = "/adicionaUsuario", method = RequestMethod.POST, headers = "Accept=application/json")
	public void adicionaUsuario(@RequestBody Usuario usuario) {
		usuarioService.adicionaUsuario(usuario);

	}

	@RequestMapping(value = "/atualizaUsuario", method = RequestMethod.PUT, headers = "Accept=application/json")
	public void atualizaUsuario(@RequestBody Usuario usuario) {
		usuarioService.atualizaUsuario(usuario);
	}

	@RequestMapping(value = "/removeUsuario/{id}", method = RequestMethod.DELETE, headers = "Accept=application/json")
	@ResponseStatus(value = HttpStatus.OK)
	public void removeUsuario(@PathVariable("id") int id) {
		usuarioService.removeUsuario(id);
	}

	@RequestMapping(value = "/autenticaUsuario", method = RequestMethod.POST, headers = "Accept=application/json")
	public boolean autenticaUsuario(@RequestBody Usuario usuario) {
		return usuarioService.autenticaUsuario(usuario);

	}

}