package com.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.entidades.AplicacaoCaptura;
import com.service.AplicacaoCapturaService;

@RestController
public class AplicacaoCapturaController {

	@Autowired
	AplicacaoCapturaService aplicacaoCapturaService;

	@RequestMapping(value = "/aplicacoesCaptura", method = RequestMethod.GET, headers = "Accept=application/json")
	public List<AplicacaoCaptura> listar() {

		List<AplicacaoCaptura> lista = aplicacaoCapturaService.listarAplicacoesCaptura();
		return lista;
	}

	@RequestMapping(value = "/aplicacaoCaptura/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
	public AplicacaoCaptura pesquisarAplicacaoCapturaPorId(@PathVariable Long id) {
		return aplicacaoCapturaService.pesquisarPorId(id);
	}

	@RequestMapping(value = "/adicionaAplicacaoCaptura", method = RequestMethod.POST, headers = "Accept=application/json")
	public void adicionaAplicacaoCaptura(@RequestBody AplicacaoCaptura AplicacaoCaptura) {
		aplicacaoCapturaService.adicionaAplicacaoCaptura(AplicacaoCaptura);

	}
}
